﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects, CustomPropertyDrawer(typeof(MapSymbolName))]
public class MapLoaderEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        MapLoading mapping = UnityEngine.Object.FindObjectOfType<MapLoading>();

        for (int i = 0; i < mapping.symbolObject.Count; i++)
        {
            if(mapping.symbolObject[i].prefab != null)
                mapping.symbolObject[i].name = mapping.symbolObject[i].prefab.name;
        }
    }
}
