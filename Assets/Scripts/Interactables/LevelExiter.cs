﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExiter : MonoBehaviour {


    public GameObject levelDoor;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            LoadNextLevel();
        }
    }

    public static void LoadNextLevel()
    {
        //UnityEngine.Print("??");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
