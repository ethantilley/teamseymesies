﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Spinner : MonoBehaviour {
    public bool spin_X, spin_Y, spin_Z;
    public float spin_Speed = 10;
    private Vector3 newRot;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float newSpinSpeed = spin_Speed * Time.deltaTime;
        newRot += new Vector3(spin_X ? newSpinSpeed : 0, spin_Y ? newSpinSpeed : 0, spin_Z ? newSpinSpeed : 0);

        transform.rotation = Quaternion.Euler(newRot);
	}
}
