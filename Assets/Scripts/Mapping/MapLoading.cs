﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
/// <summary>
/// My map loading script.
/// It loads from the "StreamingAssets" folder, 
/// and builds the level based off a specific file in that folder.
/// </summary>
public class MapLoading : MonoBehaviour
{
    public int currentLevel;
    string loadedFile = string.Empty;
    public bool completeLoad = false;
    public float spawnLocStep = 1;
    Vector3 filePosition = Vector3.zero;
    public Camera cam;
    /// <summary>
    /// A serilizable script that contrains all the data fo the building blocks for the level.
    /// This is here to used in a list and called upon specific data during the loading process.
    /// It sttores its prefab (so GameObject), that the map will span when it comes across the specified symbol char in the ascii map. 
    /// </summary>
    [System.Serializable]
    public class LevelEnititys
    {
        [MapSymbolName()] // Auto assigns name of element
        public string name;
        [Space(-15)]
        public GameObject prefab;
        public Vector3 spawnPosOffset;
        [Range(0f,360f), Tooltip("the z spawn rotation of the tile")]
        public float spawnRotation;
        [Tooltip("Input the ascii symbol that will spawn this object if found in level File")]
        public char symbol;
    }

    public List<LevelEnititys> symbolObject = new List<LevelEnititys>();

    public List<GameObject> levelItems = new List<GameObject>();

   
    public static MapLoading instance;
    /// <summary>
    /// Using a singleton pattern  here to ensure that there's only one instance of this script existing in a given scene.
    /// also good to reference in another script.
    /// </summary>
    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(this);
        }
        else
        {
            instance = this;
        }
    }

    // Use this for initialization
    void Start()
    {
        //From the get-go on initialization of this game it starts the map loading process
        GetPath();
    }

    /// <summary>
    /// Gets a stream assest path and adds it to a stream reader then calls the next func to continue the map loading process.
    /// </summary>
    public void GetPath()
    {
        completeLoad = false;
        filePosition = Vector3.zero;
        string path = Path.Combine(Application.streamingAssetsPath, "Level" + currentLevel + ".txt");
        //var path = Path.Combine(Application.streamingAssetsPath, )
        //Resources.Load<TextAsset>("StreamingAssets\\Level" + currentLevel + ".txt");

        StreamReader reader = new StreamReader(path);
        loadedFile = reader.ReadToEnd();
        LoadObjects();
    }


    /// <summary>
    /// Here Is a fuction that makes sure there there are No existing Object left before loading another map
    /// By looping through continueusly if it must to destroy and remove each object in the level items list.
    ///
    /// Then this script is the final function that reads through each symbol of the asci mapping and loads its corriponding prefab 
    /// </summary>
    /// 
    int maxWidth, maxHeight;
    Bounds levelBounds;
    GameObject currentLoadedObject;
    public void LoadObjects()
    {
        for (int i = 0; i < levelItems.Count; i++)
        {
            Destroy(levelItems[i]);
            levelItems.Remove(levelItems[i]);
        }
    
        if (levelItems.Count > 0)
        {
            LoadObjects();
            return;
        }

        if (loadedFile == string.Empty)
            return;
        int levelWidth = 0;

        for (int i = 0; i < loadedFile.Length; i++)
        {
            ++levelWidth;
            // If end of ascii line, than it steps down
            if (loadedFile[i] == '\n')
            {
                ++maxHeight;
                maxWidth = Mathf.Max(levelWidth, maxWidth);
                levelWidth = 0;
                filePosition = new Vector3(0, filePosition.y - spawnLocStep, 0);              
                
            }
            else
            {

                filePosition += new Vector3(spawnLocStep, 0, 0);

                // Search through set spawnable and see if the ascii char matches
                for (int x = 0; x < symbolObject.Count; x++)
                {
                    if (symbolObject[x].symbol == loadedFile[i])
                    {
                        Vector3 objectPos = filePosition + symbolObject[x].spawnPosOffset;
                        // Spawns object
                        GameObject newTile = Instantiate(symbolObject[x].prefab, objectPos, Quaternion.Euler(0,0, symbolObject[x].spawnRotation));
                        levelItems.Add(newTile);                    

                        currentLoadedObject = newTile;
                        break;

                    }
                }
            }

        }
        completeLoad = true;
        filePosition = Vector3.zero;

        foreach (var item in levelItems)
        {
            levelBounds.Encapsulate(item.transform.position);
        }

       // levelBounds.SetMinMax(levelItems[0].transform.position, levelItems[levelItems.Count - 1].transform.position);

        cam.transform.position = new Vector3(levelBounds.center.x, levelBounds.center.y, cam.transform.position.z);

        cam.orthographicSize = levelBounds.size.magnitude * 0.3f; // (float)(maxWidth * Screen.height / Screen.width * maxHeight) * 0.05f;
        maxWidth = maxHeight = 0;

    }

}





