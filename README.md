# 2D Platformer

2D Platformer with a story arch, in dev

## Built With

* [Unity](http://www.unity.com/) - Game Engine 
* [HackN'Plan](https://hacknplan.com/) - Task Management Softwear
* [SourceTree](https://www.sourcetreeapp.com/) - Repository softwear paired with gitlab
* [VisualStudio](https://visualstudio.microsoft.com/) - For all coding purposes

## Authors

* **Ethan Tilley** - *Lead programmer* - [@EthanTilley_](https://twitter.com/EthanTilley_)
* **Sey Atkinson** - *Lead Designer* - [@Sey_Atkinson](https://twitter.com/Sey_Atkinson)

## Acknowledgments

* Inspired by game such as Celest, super meat boy. Movies such as The fifth element.